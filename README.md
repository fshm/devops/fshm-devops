# FSHM DevOps

# Installation 
- helm repo add truecharts https://charts.truecharts.org
- helm install my-searxng truecharts/searxng --version 7.7.8 -f searx_values.yaml
- helm install my-release oci://ghcr.io/nginxinc/charts/nginx-ingress --version 1.2.0
- kubectl apply -f nginx_config.yaml
- helm repo add jetstack https://charts.jetstack.io
- helm repo update
- helm install \
        cert-manager jetstack/cert-manager \
        --namespace cert-manager \
        --create-namespace \
        --version v1.14.4 \
        --set installCRDs=true
- kubectl apply -f searx.yaml

# Tutorials
- https://artifacthub.io/packages/helm/truecharts/searxng
- https://docs.nginx.com/nginx-ingress-controller/installation/installing-nic/installation-with-helm/
- https://www.nginx.com/blog/automating-certificate-management-in-a-kubernetes-environment/
